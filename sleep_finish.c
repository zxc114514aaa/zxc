#include "linux/kernel/sched.c"

#define SIGALRM		14
int sys_sleep(unsigned int seconds)
{
	sys_signal(SIGALRM,SIG_IGN); //SIGALARM信号被忽略
	sys_alarm(seconds); //seconds时间后发SIGALARM信号
	sys_pause(); //进程挂起（等待）
	return 0;
}
