#include "linux/fs/namei.c"

long sys_getcwd(char * buf,size_t size) //找文件路径
{
	int dev,block,i,entries;
	struct m_inode * dir,*dircur,*rtdir;
	rtdir = current->root; //根目录i节点
	dir = current->pwd; //当前目录i节点
	struct buffer_head * bh,*bhcur;
	struct dir_entry * de,*decur;
	unsigned short current_node;
	char buf_[100];
	int t = 2;
	char *str1 = buf;
	char str2[20];
    str2[0] = '.';
	str2[1] = '.';
	char *str3 = str2;
	while(t--)
	{
		put_fs_byte(*(str3++),str1++);
	}
	char temp[100] ;
	temp[0] = '/';
	while(dir!=rtdir)
	{
 		bh = find_entry(&dir,buf,2,&de); //父目录
		bhcur = find_entry(&dir,buf,1,&decur); //当前目录
		current_node = decur->inode; //当前目录i项节点

 		dev = dir->i_dev; //当前设备号，bread函数要用
		dir = iget(dev,de->inode);  //把父目录i节点赋值给dir

		entries = dir->i_size / (sizeof (struct dir_entry)); //当前目录文件个数
		if (!(block = (dir)->i_zone[0])) //空文件则直接return
		return NULL;
		if (!(bh = bread(dev,block))) //父目录的数据块
		return NULL;
		i = 0;
		de = (struct dir_entry *) bh->b_data; //dir_entry为单位
		strcpy(buf_, de->name);
	}
	int len = strlen(buf_);char *p1 = buf_;
	char *p = buf;
	while (len--)
			put_fs_byte(*(p1++),p++);
	return buf;
}
