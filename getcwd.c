#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/types.h>
#include "tlpi_hdr.h"

long sys_getcwd(char * buf, size_t size){ //获取当前⼯作⽬录
	char path[BUF_MAX], cwd[BUF_MAX];
	DIR *dirp;
	struct dirent *dp;
	struct stat sb, sb_d, sb_1;
    dev_t dev;
    ino_t ino;
 
	while(1){
         //获取当前目录的文件信息
		errExit("stat");//向标准错误终端输出消息并调用 exit () 函数或 abort () 函数（如果环境变量EF_DUMPCORE设置为非空，
				 		//则会调用该函数生成核转储文件供调试用）终止程序，格式同上
    	dev = sb.st_dev;
        ino = sb.st_ino;
 
        //获取父目录的对应的目录流和父目录的文件信息
        if((dirp = opendir("..")) == NULL)
            errExit("opendir");
        if(stat("..", &sb_1) == -1)
            errExit("stat");
 
        //判断当前目录是否与父目录相同
        if(sb_1.st_dev == dev && sb_1.st_ino == ino)
           break;

        errno = 0;
        
        //在父目录对应的目录流读取条目
        while((dp = readdir(dirp)) != NULL){
            snprintf(path, BUF_MAX, "../%s", dp->d_name);
 
            if(stat(path, &sb_d) == -1)
                errExit("stat"); 
            //得到当前目录对应的条目并将目录逐渐完善
            if(dev == sb_d.st_dev && ino == sb_d.st_ino){
                memset(cwd, 0, sizeof(cwd));
                if(strcat(cwd, "/") == NULL)
                    errExit("strcat");
                if(strcat(cwd, dp->d_name) == NULL)
                    errExit("strcat");
                if(strcat(cwd, buf) == NULL)
                    errExit("strcat");
 
                if(strncpy(buf, cwd, BUF_MAX) == NULL)
                     errExit("strncpy");
                break;
             }
             
         }
 
        if(dp == NULL && errno != 0)
            errExit("readdir");

        closedir(dirp);
        chdir("..");      //改变当前目录
    }
 
    return buf;
}
