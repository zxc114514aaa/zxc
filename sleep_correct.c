#include "linux/kernel/sched.c"

#define SIGALRM		14
int sys_sleep(unsigned int seconds)
{
	sys_signal(SIGALRM,SIG_IGN);
	sys_alarm(seconds);
	sys_pause();
	return 0;
}
